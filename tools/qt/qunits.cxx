#include "units/System.h"
#include "units/grammar/Functions.h"

#include <QApplication>
#include <QCompleter>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QMainWindow>
#include <QKeySequence>
#include <QShortcut>
#include <QStringList>
#include <QStringListModel>
#include <QWidget>

#include <memory> // For shared_ptr<T>

int main(int argc, char* argv[])
{
  auto sys = units::System::createWithDefaults();
  std::string destinationUnitString(argc > 1 ? argv[1] : "meter");
  bool acceptValue = argc > 2;
  auto destinationUnit = sys->unit(destinationUnitString);
  auto compatibleUnits = sys->compatibleUnits(destinationUnit);
  std::cout << "Completing to " << destinationUnit.name() << " with " << compatibleUnits.size() << " possible matches.\n";

  QApplication app(argc, argv);
  auto* ww = new QWidget;
  auto* ly = new QHBoxLayout(ww);
  auto* ll = new QLabel("Wheelbase");
  auto* le = new QLineEdit;
  QStringList compatibleUnitList;
  for (const auto& unit : compatibleUnits)
  {
    compatibleUnitList << QString::fromStdString(unit.name());
  }
  compatibleUnitList.sort();
  QCompleter* cp;
  if (acceptValue)
  {
    cp = new QCompleter(ww);
  }
  else
  {
    cp = new QCompleter(compatibleUnitList, ww);
  }
  cp->setCompletionMode(QCompleter::UnfilteredPopupCompletion);
  ww->setLayout(ly);
  ly->addWidget(ll);
  ly->addWidget(le);
  std::ostringstream placeholder;
  if (acceptValue)
  {
    placeholder << "0";
  }
  if (!destinationUnit.dimensionless())
  {
    if (acceptValue)
    {
      placeholder << " ";
    }
    placeholder << destinationUnitString;
  }
  le->setPlaceholderText(placeholder.str().c_str());
  le->setCompleter(cp);
  QPalette pal = le->palette();
  auto* mw = new QMainWindow;
  auto* sc = new QShortcut(QKeySequence("Ctrl+Q"), mw);
  QObject::connect(sc, &QShortcut::activated, [&]() { app.closeAllWindows(); });
  mw->setCentralWidget(ww);
  mw->show();
  QObject::connect(le, &QLineEdit::textChanged, [&](const QString& text)
    {
      auto utext = text.toStdString();
      if (utext.empty())
      {
        pal.setColor(QPalette::Base, QColor("#ffffff"));
        le->setPalette(pal);
        return;
      }

      bool didParse = false;
      bool conformal = false;
      if (acceptValue)
      {
        auto measurement = sys->measurement(utext, &didParse);
        conformal = measurement.m_units.dimension() == destinationUnit.dimension();
        std::cout << measurement << " (" << measurement.m_units.dimension() << ")\n";
        if (measurement.m_value != 0.)
        {
          compatibleUnitList.clear();
          for (const auto& unit : compatibleUnits)
          {
            std::ostringstream prompt;
            prompt << measurement.m_value << " " << unit.name();
            compatibleUnitList << QString::fromStdString(prompt.str());
          }
          cp->setModel(new QStringListModel(compatibleUnitList, ww));
        }
      }
      else
      {
        auto unit = sys->unit(utext, &didParse);
        std::cout << unit << " (" << unit.dimension() << ")\n";
        conformal = unit.dimension() == destinationUnit.dimension();
      }
      if (!didParse)
      {
        pal.setColor(QPalette::Base, QColor("#ffb9b9"));
        le->setPalette(pal);
        return;
      }
      else if (!conformal)
      {
        pal.setColor(QPalette::Base, QColor("#ffdab9"));
        le->setPalette(pal);
        return;
      }

      pal.setColor(QPalette::Base, QColor("#ffffff"));
      le->setPalette(pal);
    }
  );

  app.exec();
  return 0;
}
