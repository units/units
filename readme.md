# units – Simple unit parsing and conversion

`units` is a library for unit conversion and dimensional analysis.
The goals of this library are to

+ parse user-provided strings into either units (e.g., `kg m/s^2`)
  or base dimensions (e.g., `M L/t^2`).
+ determine whether units are conformal or not and, if they are conformal,
  to convert numbers from one to the other.
+ take a set of relevant quantities (specified by units or base dimensions)
  and use Buckingham Pi theory to enumerate a set of related dimensionless
  parameters one can use to characterize relationships among them.
+ perform conversions as numerically accurate as possible
  by accepting multiple unit-to-unit (e.g., feet to inches; and feet to meters)
  conversions and use Dijkstra's algorithm to find the set which best
  avoids truncation error.
+ track substances and conversions among them to facilitate chemical reactions.
  (This is currently a low priority.)

The goals of this library do **not** include

+ performing currency conversions.
+ providing accurate historical calendar conversions of
  timestamps or time differences.
+ accounting for relativistic phenomena when high velocities
  are encountered.

## Requirements

+ C++14 or later
+ nlohmann::json (for configuration files)
+ PEGTL 2.x+ (for parsing user input and configuration files)
+ Eigen 3.4+ (for converting units and dimensional analysis)

## Examples

### Create a unit system

The first thing you should always do is create a unit system.
We provide 3 static methods to do this, but usually you will
want this one:

```cpp
  auto system = units::System::createWithDefaults();
```

This creates a unit `system` with a default set of prefixes, dimensions, units and conversions.
Once you have a system, you can use it to parse expressions and perform conversions.
The returned value is a shared pointer to an instance of `units::System`.

### Parse a string specifying a unit

Once you have a unit system, you might want to fetch a `Unit` object
for your favorite unit.

```cpp
  auto velocity = system->unit("m/s");
  auto funkyVelocity = system->unit("furlong/fortnight");

  std::cout
    << "Generally, you measure speed in " << velocity
    << " but snail races are best in " << funkyVelocity << ".\n";
```

The snippet about will print

```
  Generally, you measure speed in m·s^-1 but
  snail races are best in fur·fortnight^-1.
```

You can also get the underlying dimensions of the unit by calling `dimension()`.

```cpp
  std::cout << "The dimensions of m/s are " << velocity.dimension() << ".\n";
```

which prints

```
  The dimensions of m/s are L T^-1.
```

which indicates meters per second is measured as a length (L) per unit time (T).

### Parse a string into a measurement

A measurement combines a numeric value with the units corresponding to the value.

```cpp
  bool didParse;
  auto mv = system->measurement("42 mi/hr", &didParse);
  if (!didParse)
  {
    std::cerr << "Could not parse the measurement!\n";
    return -1;
  }
  std::cout
    << "The numeric part of the measurement is " << mv.m_value
    << " while the units are " << mv.m_units << ".\n";
```

Note that you do not have to provide a boolean to obtain a measurement, but
it is wise to do so in case your string had a typo. The example above
will print

```
  The numeric part of the measurement is 42 while the units are mi hr^-1."
```

### Convert a measurement

Now that you have units and measurements, you might want to convert a measurement
into a different set of units.

```cpp
  bool didConvert;
  auto ff = system->convert(mv, funkyVelocity, &didConvert);
  if (didConvert)
  {
    std::cout << "42 miles per hour is " << ff.m_value << " furlongs per fortnight.\n";
  }
```

The example above will print

```
  42 miles per hour is 112896 furlongs per fortnight.
```

—
Unequal weights and unequal measures are both
alike an abomination to the Lord. (Prov. 20:10).
