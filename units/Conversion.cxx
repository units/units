#include "units/Conversion.h"
#include "units/Measurement.h"

#include <stdexcept>

units_BEGIN_NAMESPACE

void Rule::transform(Measurement& mm, const Conversion& conversion, const Context& context)
{
  if (context.forward)
  {
    mm.m_units = mm.m_units / conversion.m_source.pow(context.exponent) * conversion.m_target.pow(context.exponent);
  }
  else
  {
    mm.m_units = mm.m_units / conversion.m_target.pow(context.exponent) * conversion.m_source.pow(context.exponent);
  }
  Evaluator eval = this->evaluator(conversion, context);
  mm.m_value = eval(mm.m_value);
}

FactorRule::FactorRule(double factor)
  : m_factor(factor)
{
  if (m_factor == 0.0)
  {
    throw std::invalid_argument("Unit conversion scale factor must be non-zero.");
  }
}

Rule::Evaluator FactorRule::evaluator(const Conversion& conversion, const Context& context) const
{
  (void)conversion;
  double exponent = context.exponent;
  if (context.forward)
  {
    return [this, exponent](double value) { return value * std::pow(m_factor, exponent); };
  }
  return [this, exponent](double value) { return value / std::pow(m_factor, exponent); };
}

OffsetRule::OffsetRule(double factor, double offset)
  : FactorRule(factor)
  , m_offset(offset)
{
}

Rule::Evaluator OffsetRule::evaluator(const Conversion& conversion, const Rule::Context& context) const
{
  bool absolute = false;
  if (const auto* offsetContext = dynamic_cast<const OffsetRule::Context*>(&context))
  {
    absolute = offsetContext->absolute;
  }
  double exponent = context.exponent;
  if (context.forward)
  {
    if (absolute)
    {
      return [this](double value) { return m_factor * (value + m_offset); };
    }
    return [this, exponent](double value) { return value * std::pow(m_factor, exponent); };
  }

  if (absolute)
  {
    return [this](double value) { return value / m_factor - m_offset; };
  }
  return [this, exponent](double value) { return value / std::pow(m_factor, exponent); };
}

#if 0
void OffsetRule::forward(Measurement& m, const Unit& source, const Unit& target, double exponent) const
{
  if (m.m_units.simple())
  {
    m.m_value = m_factor * (m.m_value + m_offset);
  }
  else
  {
    m.m_value = std::pow(m_factor, exponent) * m.m_value;
  }
  m.m_units = m.m_units / source.pow(exponent) * target.pow(exponent);
}

void OffsetRule::reverse(Measurement& m, const Unit& source, const Unit& target, double exponent) const
{
  if (m.m_units.simple())
  {
    m.m_value = m.m_value / m_factor - m_offset;
  }
  else
  {
    m.m_value = m.m_value / std::pow(m_factor, exponent);
  }
  m.m_units = m.m_units / target.pow(exponent) * source.pow(exponent);
}
#endif

LogRule::LogRule(double factor, double base)
  : m_factor(factor)
  , m_base(base)
{
  if (m_factor == 0.0)
  {
    throw std::invalid_argument("Unit conversion scale factor must be non-zero.");
  }
}

Rule::Evaluator LogRule::evaluator(const Conversion& conversion, const Context& context) const
{
  (void) conversion;
  double exponent = context.exponent;
  if (context.forward)
  {
    return [this, exponent](double value) { return std::pow(m_factor, exponent) * std::log(value) / std::log(m_base); };
  }
  return [this, exponent](double value) { return std::pow(value / std::pow(m_factor, exponent), m_base); };
}

#if 0
void LogRule::forward(Measurement& m, const Unit& source, const Unit& target, double exponent) const
{
  m.m_value = std::pow(m_factor, exponent) * std::log(m.m_value) / std::log(m_base);
  m.m_units = m.m_units / source.pow(exponent) * target.pow(exponent);
}

void LogRule::reverse(Measurement& m, const Unit& source, const Unit& target, double exponent) const
{
  m.m_value = std::pow(m.m_value / std::pow(m_factor, exponent), m_base);
  m.m_units = m.m_units / target.pow(exponent) * source.pow(exponent);
}
#endif

Conversion::Conversion(Unit from, Unit to, const std::shared_ptr<Rule>& rule)
  : m_rule(rule)
  , m_source(from)
  , m_target(to)
{
}

void Conversion::apply(Measurement& mm, bool forward, double exponent) const
{
  Rule::Context context{forward, exponent};
  m_rule->transform(mm, *this, context);
}

void Conversion::apply(Measurement& mm, const Rule::Context& context) const
{
  m_rule->transform(mm, *this, context);
}

Rule::Evaluator Conversion::evaluator(const Rule::Context& context) const
{
  return m_rule->evaluator(*this, context);
}

units_CLOSE_NAMESPACE
