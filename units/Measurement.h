#ifndef units_Measurement
#define units_Measurement

#include "units/Unit.h"

units_BEGIN_NAMESPACE

/// A value plus the units in which it is measured.
struct UNITS_EXPORT Measurement
{
  /// Construct a default (dimensionless, zero-valued) measurement.
  Measurement() = default;
  /// Construct a measurement with the given \a value and \a units.
  Measurement(double value, Unit units);
  /// Construct a copy of a measurement.
  Measurement(const Measurement&) = default;

  /// Return an invalid measurement.
  ///
  /// Measurements are invalid if m_value == NaN.
  /// (Also: the units of an invalid measurement will be dimensionless.)
  static Measurement invalid();

  /// Return true if the measurement is valid and false otherwise.
  bool valid() const;

  /// Return this measurement raised to the given power.
  /// This applies the \a exponent to both the value and the units.
  Measurement pow(double exponent) const;

  /// For debugging, print this measurement to the console.
  void dump() const;

  Measurement& operator *= (const Measurement& other);
  Measurement& operator /= (const Measurement& other);

  double m_value{ 0. };
  Unit m_units;
};

Measurement operator * (const Measurement& aa, const Measurement& bb);
Measurement operator / (const Measurement& aa, const Measurement& bb);
Measurement operator + (const Measurement& aa, const Measurement& bb);
Measurement operator - (const Measurement& aa, const Measurement& bb);

/// Print a measurement to the given stream \a str.
template<typename Stream>
Stream& operator << (Stream& str, const Measurement& mm)
{
  str << mm.m_value;
  if (!mm.m_units.dimensionless())
  {
    if (mm.m_units.m_powers.empty())
    {
      str << " " << mm.m_units;
    }
    else
    {
      str << "×" << mm.m_units;
    }
  }
  return str;
}

units_CLOSE_NAMESPACE

#endif // units_Measurement
