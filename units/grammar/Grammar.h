#ifndef units_grammar_Grammar_h
#define units_grammar_Grammar_h

#include "units/System.h"

#include "tao/pegtl.hpp"

#include <iostream>

units_BEGIN_NAMESPACE
namespace grammar
{

using namespace tao::pegtl;

struct UNITS_EXPORT measurement_state
{
  bool have_value{ false };
  double value{ 1.0 };
  /// When parsing dimensions (rather than units), set this to true.
  bool parsing_dimensions{ false };
  /// When parsing measurements (a value plus units), set this to true.
  bool parsing_measurement{ false };

  /// Print parse errors to the console.
  bool m_debug{ false };

  std::shared_ptr<System> system;
  Unit result;
  Dimension dimensions;
};

// Avoid a dependence on libicu:
struct UNITS_EXPORT white_space :
  sor<
    one<' ', '\t', '\n', '\v', '\f', '\r'>,
    TAO_PEGTL_ISTRING(" "), // non-breaking space, U+00a0
    TAO_PEGTL_ISTRING(" "), // en space, U+2002
    TAO_PEGTL_ISTRING(" "), // em space, U+2003
    TAO_PEGTL_ISTRING(" "), // three-per-em space, U+2004
    TAO_PEGTL_ISTRING(" "), // four-per-em space, U+2005
    TAO_PEGTL_ISTRING(" "), // six-per-em space, U+2006
    TAO_PEGTL_ISTRING(" "), // figure space, U+2007
    TAO_PEGTL_ISTRING(" "), // punctuation space, U+2008
    TAO_PEGTL_ISTRING(" "), // thin space, U+2009
    TAO_PEGTL_ISTRING(" "), // hair space, U+200a
    TAO_PEGTL_ISTRING(" "), // narrow non-breaking space, U+202f
    TAO_PEGTL_ISTRING(" "), // medium mathematical space, U+205f
    TAO_PEGTL_ISTRING("␠"), // symbol for space, U+2420
    TAO_PEGTL_ISTRING("　"), // ideographic space, U+3000
    TAO_PEGTL_ISTRING("𑽈") // Kawi punctation space, U+11f48
    // TAO_PEGTL_ISTRING(" "), // Ogham space mark, U+1680
  > {};

// -----
// This fragment (floating-point parse rules) has been
// adapted from PEGTL's example double:
// Copyright (c) 2014-2018 Dr. Colin Hirsch and Daniel Frey
// Visit https://github.com/taocpp/PEGTL/ for license information.
struct UNITS_EXPORT plus_minus : opt< one< '+', '-' > > {};
struct UNITS_EXPORT dot : one< '.' > {};
struct UNITS_EXPORT inf : seq< TAO_PEGTL_ISTRING("inf"),
                  opt< TAO_PEGTL_ISTRING("inity") > > {};
struct UNITS_EXPORT nan : seq< TAO_PEGTL_ISTRING("nan"),
                  opt< one< '(' >,
                       plus< alnum >,
                       one< ')' > > > {};
template< typename D >
struct number :
  if_then_else< dot,
                plus< D >,
                seq< plus< D >, opt< dot, star< D > > >
                > {};
struct UNITS_EXPORT e : one< 'e', 'E' > {};
struct UNITS_EXPORT p : one< 'p', 'P' > {};
struct UNITS_EXPORT exponent : seq< plus_minus, plus< tao::pegtl::digit > > {};
struct UNITS_EXPORT decimal : seq< number< tao::pegtl::digit >, opt< e, exponent > > {};
struct UNITS_EXPORT hexadecimal : seq< one< '0' >, one< 'x', 'X' >, number< xdigit >,
                          opt< p, exponent > > {};
struct UNITS_EXPORT value : seq< plus_minus, sor< hexadecimal, decimal, inf, nan > > {};
// -----

// A keyword indicating translation from some baseline.
struct UNITS_EXPORT shift_kw : seq<
  star<white_space>,
  sor<
    one<'@'>,
    TAO_PEGTL_ISTRING("after"),
    TAO_PEGTL_ISTRING("from"),
    TAO_PEGTL_ISTRING("ref"),
    TAO_PEGTL_ISTRING("since")
  >,
  star<white_space>> {};

struct UNITS_EXPORT divide_kw : seq<
  star<white_space>,
  sor<
    one<'/'>,
    TAO_PEGTL_ISTRING("per")
  >,
  star<white_space>> {};

struct UNITS_EXPORT multiply_kw :
  seq<
    sor<
      seq<
        star<white_space>,
        sor<
          TAO_PEGTL_ISTRING("*"),
          TAO_PEGTL_ISTRING("×"),
          TAO_PEGTL_ISTRING("·")
        >
      >,
      one<' '>
    >,
    star<white_space>
  > {};

// Accept either a^b or a**b.
struct UNITS_EXPORT exponent_kw : sor<one<'^'>, TAO_PEGTL_ISTRING("**")> {};

// Parentheses can group expressions
struct UNITS_EXPORT lparen_kw : one<'('> {};
struct UNITS_EXPORT rparen_kw : one<')'> {};

struct UNITS_EXPORT basic_first :
  sor<
    ranges<'a', 'z', 'A', 'Z'>,
    TAO_PEGTL_ISTRING("‘"),
    TAO_PEGTL_ISTRING("’"),
    TAO_PEGTL_ISTRING("“"),
    TAO_PEGTL_ISTRING("”"),
    TAO_PEGTL_ISTRING("°"),
    TAO_PEGTL_ISTRING("Θ"),
    TAO_PEGTL_ISTRING("θ"),
    TAO_PEGTL_ISTRING("φ"),
    TAO_PEGTL_ISTRING("Ω"),
    TAO_PEGTL_ISTRING("㏀"),
    TAO_PEGTL_ISTRING("㏁"),
    TAO_PEGTL_ISTRING("℧"),
    TAO_PEGTL_ISTRING("℉"),
    TAO_PEGTL_ISTRING("℃"),
    TAO_PEGTL_ISTRING("K"),
    TAO_PEGTL_ISTRING("Å"),
    TAO_PEGTL_ISTRING("µ")
  > {};

struct UNITS_EXPORT basic_expression :
  seq<
    basic_first,
    star<
      sor<
        basic_first,
        one<'_'>,
        one<'-'>
      >
    >
  > {};

struct UNITS_EXPORT power_expression :
  sor<
    seq<basic_expression, exponent_kw, exponent>,
    // seq<basic_expression, exponent>,
    basic_expression
  > {};

struct UNITS_EXPORT product_expression;

struct UNITS_EXPORT paren_expression : seq<lparen_kw, seq<product_expression>, rparen_kw> {};

struct UNITS_EXPORT product_expression :
  sor<
    paren_expression,
    seq<power_expression, multiply_kw, product_expression>,
    seq<power_expression,   divide_kw, product_expression>,
    power_expression
  > {};

struct UNITS_EXPORT shifted_expression :
  sor<
    seq<product_expression, shift_kw, value>,
    //, seq<product_expression, shift_kw, timestamp>
    product_expression
  > {};

struct UNITS_EXPORT units : must<shifted_expression, eof> {};
struct UNITS_EXPORT measurement
  : must<
      seq<
        value,
        star<white_space>,
        opt<
          shifted_expression
        >
      >,
      eof
    > {};

enum class op_order : int
{
};

struct UNITS_EXPORT op_token
{
  op_order p;
  char opcode;
};

struct UNITS_EXPORT operators
{
  operators()
  {
    this->insert("^", op_order(4), '^');
    this->insert("**", op_order(4), '^');
    this->insert("*", op_order(5), '*');
    this->insert("×", op_order(5), '*');
    this->insert("·", op_order(5), '*');
    this->insert("/", op_order(5), '/');
    this->insert("+", op_order(6), '+');
    this->insert("-", op_order(6), '-');
  }

  void insert( const std::string& name, const op_order p, char sym)
  {
    assert(!name.empty());
    m_ops.insert({name, {p, sym}});
  }

  const std::map<std::string, op_token>& ops() const { return m_ops; }

  std::map<std::string, op_token> m_ops;
};

struct UNITS_EXPORT stacks
{
};

struct UNITS_EXPORT unit_kw
{
  using analyze_t = analysis::generic< analysis::rule_type::ANY >;
  template<
    apply_mode,
    rewind_mode,
    template< typename... > class Action,
    template< typename... > class Control,
    typename Input,
    typename... States >
  static bool match(Input& in, const operators& b, stacks& stack, System* sys, States&&... /*unused*/)
  {
    std::string taken;
    // Look for the longest match from our dictionary of unit (or prefix+unit) tokens.
    return unit_kw::matchInternal(in, sys, taken, false);
  }

  template<typename Input>
  static bool matchInternal(Input& in, System* sys, std::string& taken, bool unitsOnly)
  {
    struct Match
    {
      std::string input;
      InternalUnit* unit{ nullptr };
      InternalPrefix* prefix{ nullptr };
    } match1, match2;

    // Reset taken and match only units (no prefixes):
    unit_kw::updateMatchFromNGrams(in, sys, taken, sys->m_unitNGrams, match1, false);
    // Reset taken and match prefix + unit pairs:
    taken.clear();
    unit_kw::updateMatchFromNGrams(in, sys, taken, sys->m_prefixNGrams, match2, false);
    unit_kw::updateMatchFromNGrams(in, sys, taken, sys->m_unitNGrams, match2, true);

    if (match2.unit && match2.input.size() > match1.input.size())
    {
      // Choose match2 if it is longer.
      match1 = match2;
    }

    // Now the match (if any) is the longest possible match.
    if (match1.unit)
    {
      in.bump(match1.input.size());
      return true;
    }
    return false;
  }

  template<typename Input, typename NGrams, typename Match>
  static void updateMatchFromNGrams(
    Input& in, System* sys, std::string& taken, const NGrams& ngrams, Match& match, bool startAtMatch)
  {
    std::size_t offset = (startAtMatch ? match.input.size() : 0);
    if (offset > 0)
    {
      taken = taken.substr(offset);
    }
    // Peek the first 1 or 2 characters.
    // We index by 1-grams and 2-grams, but not more (yet).
    std::set<Entity*> visited; // Never visit the same entity twice.
    for (std::size_t nn = 1; nn <= 2; ++nn)
    {
      if (taken.size() < nn)
      {
        if (in.size(taken.size() + 1) > taken.size())
        {
          taken += in.peek_char(offset + taken.size());
        }
        else
        {
          // end of input; we cannot peek far enough to get an nn-gram.
          break;
        }
      }
      auto ngram = taken.substr(0, nn);
      auto it = ngrams.lower_bound(ngram);
      bool done = (it != ngrams.end());
      for (; it != ngrams.end(); ++it)
      {
        if (it->first > taken)
        {
          // No more matches.
          break;
        }
        if (visited.find(it->second) != visited.end())
        {
          // Only visit the same entity once, no matter how many
          // n-grams reference it.
          continue;
        }
        visited.insert(it->second);
        unit_kw::updateMatchWithEntity(in, taken, it->second, match, offset);
        // std::cerr << "  Try matching _" << taken << "_ to _" << it->second->m_name.data() << "_? " << (match.unit == reinterpret_cast<InternalUnit*>(it->second) ? "Y" : "N") << "\n";
      }
    }
  }

  template<typename Input, typename Entity, typename Match>
  static void updateMatchWithName(Input& in, std::string& taken, Entity* entity, const std::string& name, Match& match, std::size_t offset)
  {
    bool havePriorMatch = false;
    if (std::is_same<Entity, ::units::InternalUnit>::value)
    {
      havePriorMatch = !!match.unit;
    }
    else
    {
      havePriorMatch = !!match.prefix;
    }
    if (havePriorMatch && match.input.size() - offset > name.size())
    {
      // A prior candidate matched with a longer string that we can.
      return;
    }
    if (name.size() > taken.size())
    {
      if (in.size(offset) >= name.size())
      {
        // Add to taken by peeking
        for (std::size_t ii = taken.size(); ii < name.size(); ++ii)
        {
          taken += in.peek_char(offset + ii);
          // std::cerr << "    Peek " << ii << " _" << taken << "_\n";
        }
      }
      else
      { // name is longer than the available input. We cannot match.
        return;
      }
    }
    // If we get here, taken is at least as long as name
    // AND name is at least as long as any prior match.
    if (name == taken.substr(0, name.size()))
    {
      if (std::is_same<Entity, ::units::InternalUnit>::value)
      {
        match.unit = reinterpret_cast<::units::InternalUnit*>(entity);
      }
      else
      {
        match.prefix = reinterpret_cast<::units::InternalPrefix*>(entity);
      }
      match.input = match.input.substr(0, offset) + name;
    }
  }

  template<typename Input, typename Entity, typename Match>
  static void updateMatchWithEntity(Input& in, std::string& taken, Entity* entity, Match& match, std::size_t offset)
  {
    if (!entity) { return; }
    if (entity->m_name.valid())
    {
      unit_kw::updateMatchWithName(in, taken, entity, entity->m_name.data(), match, offset);
    }
    if (!entity->m_noPlural)
    {
      std::string plural = entity->m_plural.valid() ? entity->m_plural.data() : (entity->m_name.data() + "s");
      unit_kw::updateMatchWithName(in, taken, entity, plural, match, offset);
    }
    if (entity->m_symbol.valid())
    {
      unit_kw::updateMatchWithName(in, taken, entity, entity->m_symbol.data(), match, offset);
    }
    for (const auto& alias : entity->m_nameAliases)
    {
      unit_kw::updateMatchWithName(in, taken, entity, alias.data(), match, offset);
    }
    for (const auto& alias : entity->m_symbolAliases)
    {
      unit_kw::updateMatchWithName(in, taken, entity, alias.data(), match, offset);
    }
  }
};

struct UNITS_EXPORT infix_operator
{
  using analyze_t = analysis::generic< analysis::rule_type::ANY >;
  template<
    apply_mode,
    rewind_mode,
    template< typename... > class Action,
    template< typename... > class Control,
    typename Input,
    typename... States >
  static bool match(Input& in, const operators& b, stacks& stack, System* sys, States&&... /*unused*/)
  {
    // Look for the longest match of the input against the operators in the operator map.
    return infix_operator::matchInternal(in, b, stack, sys, std::string());
  }

private:

  template< typename Input >
  static bool matchInternal(Input& in, const operators& b, stacks& s, System* sys, std::string taken)
  {
    if (in.size(taken.size() + 1) > taken.size())
    {
      taken += in.peek_char(taken.size());
      const auto it = b.ops().lower_bound( taken );
      if (it != b.ops().end())
      {
        if (infix_operator::matchInternal( in, b, s, sys, taken))
        {
          // This happens after the conditional below returns true.
          return true;
        }
        // Have we taken enough look-ahead characters to find a match?
        if (it->first == taken)
        {
          // While we are at it, this rule also performs the task of what would
          // usually be an associated action: To push the matched operator onto
          // the operator stack.
          // s.push(it->second);
          in.bump(taken.size());
          return true;
        }
      }
    }
    return false;
  }
};

struct unit_expression;
struct UNITS_EXPORT unit_infix_op
  : sor<
      one<'*','/'>,
      TAO_PEGTL_ISTRING("×"),
      TAO_PEGTL_ISTRING("·"),
      TAO_PEGTL_ISTRING("÷")
    > {};
struct UNITS_EXPORT unit_parenthetical : if_must<one<'('>, unit_expression, star<white_space>, one<')'>> {};
struct UNITS_EXPORT unit_exponential : if_must<exponent_kw, star<white_space>, exponent> {};
struct UNITS_EXPORT unit_combined_separator : seq<unit_exponential, star<white_space>, unit_infix_op> {};
struct UNITS_EXPORT unit_exponential_separator : unit_exponential {};
struct UNITS_EXPORT unit_infix_separator : unit_infix_op {};
struct UNITS_EXPORT unit_separator
  : opt<
      sor<
        unit_combined_separator,
        unit_exponential_separator,
        unit_infix_separator
      >
    >
{
  using superclass = opt<
    sor<
      unit_combined_separator,
      unit_exponential_separator,
      unit_infix_separator
    >
  >;
  using analyze_t = analysis::generic< analysis::rule_type::ANY >;
  template<
    apply_mode A,
    rewind_mode R,
    template< typename... > class Action,
    template< typename... > class Control,
    typename Input,
    typename... States >
  static bool match(Input& in, States&&... st)
  {
    bool didMatch = superclass::match<A, R, Action, Control>(in, st...);
#if 0
    if (didMatch)
    {
      std::cout << "Matched separator, input at " << in.current() << "\n";
    }
#endif
    return didMatch;
  }
};
struct UNITS_EXPORT unit_expression
  : seq<
      list<
        // A unit expression's "node" type is either a parenthetical or a single unit+prefix.
        sor<
          unit_parenthetical,
          unit_kw
        >,
        // A separator between any number of the above may be an exponential and/or an infix operand
        unit_separator,
        // Padding allowed between unit expression nodes.
        white_space
      >,
      // Any expression can end with an exponential (but never an infix operand)
      opt<unit_exponential_separator>
    > {};

struct expression; // Forward declared for parenthetical atomic:
struct UNITS_EXPORT parenthetical_atomic : if_must<one<'('>, expression, one<')'>> {};
struct UNITS_EXPORT measurement_atomic : seq<value, star<white_space>, opt<unit_expression>> {};
struct UNITS_EXPORT atomic : sor<measurement_atomic, parenthetical_atomic> {};
struct UNITS_EXPORT expression : list<atomic, infix_operator, white_space> {};

/// A grammar that parses measurements.
struct UNITS_EXPORT measurement_grammar : must<expression, star<white_space>, eof> {};

/// A grammar that parses units.
struct UNITS_EXPORT unit_grammar : must<unit_expression, star<white_space>, eof> {};

#if 0
/// A gramar that parses dimensions.
struct UNITS_EXPORT dimension_grammar : dimension_expression
  : seq<
      list<
        // A unit expression's "node" type is either a parenthetical or a single unit+prefix.
        sor<
          dimension_parenthetical,
          dimension_kw
        >,
        // A separator between any number of the above may be an exponential and/or an infix operand
        dimension_separator,
        // Padding allowed between unit expression nodes.
        white_space
      >,
      // Any expression can end with an exponential (but never an infix operand)
      opt<dimension_exponential_separator>
    > {};
#endif // 0

} // namespace grammar
units_CLOSE_NAMESPACE

#endif // units_grammar_Grammar_h
