#include "units/Measurement.h"

#include "units/Converter.h"
#include "units/System.h"

#include <iostream>

units_BEGIN_NAMESPACE

Measurement::Measurement(double value, Unit units)
  : m_value(value)
  , m_units(units)
{
}

Measurement Measurement::invalid()
{
  return Measurement(std::numeric_limits<double>::quiet_NaN(), Unit());
}

bool Measurement::valid() const
{
  return m_value != std::numeric_limits<double>::quiet_NaN();
}

Measurement Measurement::pow(double exponent) const
{
  Measurement result(std::pow(m_value, exponent), m_units.pow(exponent));
  return result;
}

void Measurement::dump() const
{
  std::cout << *this << "\n";
}

Measurement& Measurement::operator *= (const Measurement& other)
{
  m_value *= other.m_value;
  m_units = m_units * other.m_units;
  return *this;
}

Measurement& Measurement::operator /= (const Measurement& other)
{
  m_value /= other.m_value;
  m_units = m_units / other.m_units;
  return *this;
}

Measurement operator * (const Measurement& aa, const Measurement& bb)
{
  Measurement result(aa);
  result *= bb;
  return result;
}

Measurement operator / (const Measurement& aa, const Measurement& bb)
{
  Measurement result(aa);
  result /= bb;
  return result;
}

Measurement operator + (const Measurement& aa, const Measurement& bb)
{
  auto result = Measurement::invalid();
  auto* sys = aa.m_units.system();
  if (!sys)
  {
    sys = bb.m_units.system();
  }
  else if (sys != bb.m_units.system())
  {
    // Measurements must be from the same system.
    return result;
  }

  if (!sys)
  {
    if (aa.m_units == bb.m_units)
    {
      result = Measurement(aa.m_value + bb.m_value, aa.m_units);
    }
    return result;
  }

  auto convert = sys->convert(bb.m_units, aa.m_units);
  if (!convert)
  {
    // bb cannot be converted to match the units of aa.
    return result;
  }
  result = Measurement(aa.m_value + convert->transform(bb.m_value), aa.m_units);
  return result;
}

Measurement operator - (const Measurement& aa, const Measurement& bb)
{
  auto result = Measurement::invalid();
  auto* sys = aa.m_units.system();
  if (!sys)
  {
    sys = bb.m_units.system();
  }
  else if (sys != bb.m_units.system())
  {
    // Measurements must be from the same system.
    return result;
  }

  if (!sys)
  {
    if (aa.m_units == bb.m_units)
    {
      result = Measurement(aa.m_value + bb.m_value, aa.m_units);
    }
    return result;
  }

  auto convert = sys->convert(bb.m_units, aa.m_units);
  if (!convert)
  {
    // bb cannot be converted to match the units of aa.
    return result;
  }
  result = Measurement(aa.m_value - convert->transform(bb.m_value), aa.m_units);
  return result;
}

units_CLOSE_NAMESPACE
