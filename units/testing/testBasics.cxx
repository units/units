#include "units/System.h"
#include "units/Measurement.h"
#include "units/string/Token.h"
#include "units/grammar/Functions.h"

#include "tao/pegtl/analyze.hpp"

#include <iostream>

bool testFactor(
  const units::System* sys,
  const std::string& u1s,
  const std::string& u2s,
  double expected)
{
  bool didParse = false;
  auto u1 = sys->unit(u1s, &didParse);
  if (!didParse)
  {
    std::cerr << "    ERROR: Could not parse " << u1s << ".\n";
    return false;
  }

  auto u2 = sys->unit(u2s, &didParse);
  if (!didParse)
  {
    std::cerr << "    ERROR: Could not parse " << u2s << ".\n";
    return false;
  }

  double u12 = u2.factor(u1);
  std::cout << "  Factor of " << u1 << " in " << u2 << " = " << u12 << "\n";
  if (std::abs(u12 - expected) > 1e-10)
  {
    std::cerr << "    ERROR: Expected " << expected << "\n";
    return false;
  }
  return true;
}

bool testUnitFactorization(units::System* sys)
{
  std::cout << "Unit factor computations:\n";
  bool ok = true;
  // Test simple factorization:
  ok &= testFactor(sys, "g m^2", "g m^3 / s^2", 1);
  ok &= testFactor(sys, "g m^2", "g^-3 m^-4", -2);
  ok &= testFactor(sys, "m^-2", "g / m^2", 1);

  // Test simple factorization with mixed signs and absent factors:
  ok &= testFactor(sys, "g m^2", "g / m^2", 0);
  ok &= testFactor(sys, "g m^2", "m^-2", 0);

  // Unit::factor() should not attempt to apply conversions:
  ok &= testFactor(sys, "m", "N", 0); // not 1.
  ok &= testFactor(sys, "m", "N m", 1); // not 2.

  // Test fractional factor:
  ok &= testFactor(sys, "m^2 ft", "m^-3 ft^-4", -1.5);
  ok &= testFactor(sys, "m^2 ft", "m^3 ft^-4", 0);

  std::cout << "  Unit factor computation tests: " << (ok ? "good" : "bad") << "\n\n";
  return true;
}

bool testDefaultSys()
{
  auto sys = units::System::createWithDefaults();

  std::string input = "m";
  bool parsedOK = false;
  auto unit = sys->unit(input, &parsedOK);
  if (!parsedOK || unit.dimensionless())
  {
    std::cerr << "Didn't parse " << input << std::endl;
    return false;
  }

  // Sanity check
  if (unit.system() == nullptr)
  {
    std::cerr << "ERROR: unit.system() is null\n";
    return false;
  }

  // Test that one string is returned for every unit.
  auto allUnits = sys->allUnits();
  std::cout << "All default units:\n";
  for (const auto& label : allUnits)
  {
    std::cout << "  " << label << "\n";
  }
  if (sys->m_units.size() != allUnits.size())
  {
    std::cerr << "ERROR: allUnits() does not have expected length.\n";
    return false;
  }

  return true;
}

bool testPluralWithPrefix(
  const std::shared_ptr<units::System>& sys,
  const std::string& unitStr,
  const std::string expectedCanonical,
  double expectedMultiplier)
{
  std::cout << "    Test " << unitStr << "\n";
  bool ok = true;
  bool didParse;
  auto unit = sys->unit(unitStr, &didParse);
  if (!didParse)
  {
    std::cerr << "ERROR: No plural \"" << unitStr << "\" unit.\n";
    ok = false;
  }
  else
  {
    if (unit.canonical() != sys->unit(expectedCanonical))
    {
      std::cerr << "ERROR: " << unitStr << " base unit is not " << expectedCanonical << "\n";
      ok = false;
    }
    if (unit.multiplier() != expectedMultiplier)
    {
      std::cerr << "ERROR: " << unitStr << " multiplier is " << unit.multiplier() << "\n";
      ok = false;
    }
  }
  return ok;
}

bool testPluralForms()
{
  std::cout << "Testing plural forms\n";
  auto sys = units::System::createWithDefaults();
  bool ok = true;
  bool didParse;

  // Test that default-plural dimensions are registered
  auto dim = sys->dimension("length", &didParse);
  if (!didParse) { std::cerr << "ERROR: No singular \"length\" dimension.\n"; }
  ok &= didParse;

  dim = sys->dimension("lengths", &didParse);
  if (!didParse) { std::cerr << "ERROR: No plural \"lengths\" dimension.\n"; }
  ok &= didParse;

  // Test that user-provided-plural dimensions are registered
  dim = sys->dimension("energy", &didParse);
  if (!didParse) { std::cerr << "ERROR: No singular \"energy\" dimension.\n"; }
  ok &= didParse;

  dim = sys->dimension("energies", &didParse);
  if (!didParse) { std::cerr << "ERROR: No plural \"energies\" dimension.\n"; }
  ok &= didParse;

  // Test that default-plural units are registered
  auto unit = sys->unit("meter", &didParse);
  if (!didParse) { std::cerr << "ERROR: No singular \"meter\" unit.\n"; }
  ok &= didParse;

  unit = sys->unit("meters", &didParse);
  if (!didParse) { std::cerr << "ERROR: No plural \"meters\" unit.\n"; }
  ok &= didParse;
  if (unit.dimension() != sys->dimension("length"))
  {
    std::cerr
      << "ERROR: Improper plural of meters as "
      << unit.name() << ", " << unit.dimension() << ".\n";
    ok = false;
  }

  // Test that user-provided-plural units are registered
  unit = sys->unit("inch", &didParse);
  if (!didParse) { std::cerr << "ERROR: No singular \"inch\" unit.\n"; }
  ok &= didParse;

  unit = sys->unit("inches", &didParse);
  if (!didParse) { std::cerr << "ERROR: No plural \"inches\" unit.\n"; }
  ok &= didParse;

  // Regression: test that plurals with a prefix are properly matched.
  ok &= testPluralWithPrefix(sys, "kilograms", "g", 1000);
  ok &= testPluralWithPrefix(sys, "kgrams", "g", 1000);
  ok &= testPluralWithPrefix(sys, "kigrams", "g", 1024);
  ok &= testPluralWithPrefix(sys, "kibigrams", "g", 1024);
  // Nuke it from orbit just to be sure: (test with singular as well)
  ok &= testPluralWithPrefix(sys, "kilogram", "g", 1000);
  ok &= testPluralWithPrefix(sys, "kgram", "g", 1000);
  ok &= testPluralWithPrefix(sys, "kigram", "g", 1024);
  ok &= testPluralWithPrefix(sys, "kibigram", "g", 1024);

  std::cout << "  Plural form tests: " << (ok ? "good" : "bad") << "\n\n";
  return ok;
}

bool testWhitespaceParsing()
{
  std::cout << "Testing whitespace parsing\n";
  auto sys = units::System::createWithDefaults();
  bool ok = true;
  bool didParse;

  // Test that dimensions with whitespaces in their names are parsed.
  auto dim = sys->dimension("electric current", &didParse);
  if (!didParse) { std::cerr << "ERROR: Could not find dimension with whitespace in its name.\n"; }
  ok &= didParse;

  std::vector<std::string> tests{
    "W/m*K",
    "W / m * K",
    "W / m* K",
    "W / m *K",
    "W/ m* K"
  };
  units::Unit unit;
  for (const auto& test : tests)
  {
    unit = sys->unit(test, &didParse);
    if (!didParse) { std::cerr << "ERROR: Could not parse \"" << test << "\" (whitespace check).\n"; }
    ok &= didParse;
  }

  std::cout << "  Whitespace parsing tests: " << (ok ? "good" : "bad") << "\n\n";
  return ok;
}

bool testParenthesisParsing()
{
  std::cout << "Testing parenthesis parsing\n";
  auto sys = units::System::createWithDefaults();
  bool ok = true;
  bool didParse;

  std::size_t numberOfIssues = tao::pegtl::analyze< units::grammar::measurement_grammar >();
  if (numberOfIssues > 0)
  {
    std::cerr << "ERROR: " << numberOfIssues << " issues detected with measurement_grammar.\n";
    ok = false;
  }

  auto milli = sys->prefix("milli");
  auto kilo = sys->prefix("kilo");
  auto gram = sys->unit("gram");
  auto watt = sys->unit("Watt");
  auto meter = sys->unit("m");
  auto kelvin = sys->unit("K");
  auto hr = sys->unit("hr");
  auto min = sys->unit("min");
  auto sec = sys->unit("s");
  auto degF = sys->unit("degF");
  auto hp = sys->unit("hp");
  auto inch = sys->unit("inch");

  std::vector<std::pair<std::string, units::Measurement>> tests{
    // Test parenthetical expressions with variations of exponentiation, order,
    // multiplication, division, and whitespace:
    { "2.3 W (hr / m^2 )K",                      {2.3, watt*hr*kelvin/meter/meter}},
    { "2.3 W (s / m )^2 s × K^3",                {2.3, watt*(sec/meter).pow(2)*sec*kelvin.pow(3)}},
    { "2.3 (kW)/m^2*K",                          {2.3, kilo*watt/meter.pow(2)/kelvin}},
    { "2.3 (W/m^2*K)",                           {2.3, watt/meter.pow(2)/kelvin}},
    { "2.3 W / (m^2 * K)",                       {2.3, watt/meter.pow(2)/kelvin}},
    { "2.3 W / m^2* K",                          {2.3, watt/meter.pow(2)/kelvin}},
    { "2.3 W hr / m^2 *K",                       {2.3, watt*hr/meter.pow(2)/kelvin}},
    { "2.3 W/ m^2* K ",                          {2.3, watt/meter.pow(2)/kelvin}},
    { "2.3 W/ m^2/ K",                           {2.3, watt/meter.pow(2)/kelvin}},
    { "2.3 kW / mm^2/ Kelvin",                   {2.3, kilo*watt/(milli*meter).pow(2)/kelvin}},
    { "2.3 kW / mm^2 K",                         {2.3, kilo*watt/(milli*meter).pow(2)/kelvin}},
    { "2.3 mm^2 K / degF",                       {2.3, (milli*meter).pow(2)*kelvin/degF}},
    { "2.3 (kg / s)^2 / (kg/min)^1",             {2.3, (kilo*gram/sec).pow(2)/(kilo*gram/min)}},
    { "2.3 (kg (m / s)^2)^3 / (kg/min)^2",       {2.3, (kilo*gram * (meter/sec).pow(2)).pow(3)/(kilo*gram/min).pow(2)}},
    { "2.3 ((m / s)^2 * kg)^3 / (kg/min)^2 ",    {2.3, (kilo*gram * (meter/sec).pow(2)).pow(3)/(kilo*gram/min).pow(2)}},
    { "2   Watt / meter^2/ Kelvin * 5",          {10., watt/meter.pow(2)/kelvin}},
    { "5 * 2 Watt / meter ^2 / Kelvin",          {10., watt/meter.pow(2)/kelvin}},
    // Test addition of compatible units:
    { "2.3 Watt / meter^2/ Kelvin + 2.40326e-06 hp / inch^2 / degF", {7.3, watt/meter.pow(2)/kelvin}},
    // Test addition/subtraction of incompatible units:
    { "2.3 Watt / meter^2/ Kelvin + 5 ",         units::Measurement::invalid() },
    { "2.3 Watt / meter^2/ Kelvin - 5 degF",     units::Measurement::invalid() }
  };
  units::Unit unit;
  for (const auto& test : tests)
  {
    std::cout << "About to parse " << test.first << "\n  Expect " << test.second << "\n";
    units::grammar::measurement_state state;
    // Uncomment this to see the parse tree in graphviz form (if successful):
    // state.m_debug = true;
    state.system = sys;
    state.parsing_measurement = true;
    didParse = units::grammar::parseUnits2(test.first, state);
    if (!didParse) { std::cerr << "  ERROR: Could not parse \"" << test.first << "\" (parenthesis check).\n"; }
    ok &= didParse;
    std::cout << "  Parsed as " << state.value << " " << state.result << "\n";

    bool valuesMatch = std::isnan(test.second.m_value) ?
      std::isnan(state.value) :
      std::fabs(state.value - test.second.m_value) < 1e-5;
    bool unitsMatch = (state.result == test.second.m_units);
    if (!valuesMatch) { std::cerr << "  ERROR: Value mismatch " << state.value << " != " << test.second.m_value << "\n"; }
    if (!unitsMatch) { std::cerr << "  ERROR: Unit mismatch " << state.result << " != " << test.second.m_units << "\n"; }
    ok &= unitsMatch && valuesMatch;
    std::cout << "Finished parse\n";
  }

  std::cout << "  Parenthesis parsing tests: " << (ok ? "good" : "bad") << "\n";
  return ok;
}

template<typename T>
void dump(const std::multimap<std::string, T*>& ngrams, std::size_t maxLength, const std::string& title)
{
  std::cout << "## " << title << " N-Grams ##\n";
  std::cout << "Max length: " << maxLength << "\n\n";
  for (const auto& entry : ngrams)
  {
    int n1 = static_cast<unsigned char>(entry.first[0]);
    if (entry.first.size() > 1)
    {
      int n2 = static_cast<unsigned char>(entry.first[1]);
      std::cout << "  '" << n1 << ", " << n2 << "': ";
    }
    else
    {
      std::cout << "  '" << n1 << "    ': ";
    }
    std::cout << entry.second->m_name.data() << " " << entry.second->m_symbol.data() << "\n";
  }
  std::cout << "##\n";
}

bool dumpNGrams()
{
  auto sys = units::System::createWithDefaults();
  dump(sys->m_prefixNGrams, sys->m_maxPrefixLength, "Prefix");
  dump(sys->m_unitNGrams, sys->m_maxUnitLength, "Unit");
  return true;
}

int testBasics(int argc, char* argv[])
{
  using namespace units::string::literals; // for ""_token
  bool ok = true;

  ok &= testDefaultSys();
  ok &= dumpNGrams();

  // Construct an empty unit system.
  auto sys = std::make_shared<units::System>();

  // Add a prefix to it.
  auto milli = sys->createPrefix(10, -3, "milli", "m");
  auto kilo = sys->createPrefix(10, +3, "kilo", "k");

  // Add some (internal, basic) dimensions to it:
  auto mass = sys->createDimension("mass", "M", "Mass of a substance.");
  auto length = sys->createDimension("length", "L", "Distance in a metric field.");
  auto time = sys->createDimension("time", "T", "Distance in a temporal field.");

  // Do "math" with the dimensions:
  auto speed = length / time;
  std::cout << "Speed has basic dimensions " << speed << "\n";
  auto force = mass * length / time.pow(2);
  std::cout << "Force has basic dimensions " << force << "\n";
  auto force2 = mass * speed / time;
  std::cout << "Force via speed also has basic dimensions " << force2 << "\n";
  if (force != force2)
  {
    std::cerr << "ERROR: Expected force dimensions to match.\n";
    ok = false;
  }

  auto none = units::string::Token::invalid();
  auto gram = sys->createUnit(mass, "g", "gram", "Metric (cgs) measure of mass.");
  auto meter = sys->createUnit(length, "m", "meter", "Metric (mks) measure of length.");
  auto second = sys->createUnit(time, "s", "second", "Metric (mks and cgs) measure of time.");
  auto newton = sys->createUnit(force, "N", "Newton", "Metric (mks) measure of force.");
  auto foot = sys->createUnit(length, "ft", "foot", "feet", "Imperial measure of length derived from a monarch's shoe size.");
  auto inch = sys->createUnit(length, "in", "inch", "inches", "Imperial measure of length.");
  auto furlong = sys->createUnit(length, none, "furlong", "Horsie racing distance.");
  auto poundsforce = sys->createUnit(force, "lbf", "pounds-force", "Imperial measure of force.");
  auto millinewton = gram * meter / second;
  std::cout << "A millinNewton is " << millinewton << " with dimension " << millinewton.dimension() << "\n";
  auto fullnewton = (kilo * gram) * meter / second;
  std::cout << "A Newton is " << fullnewton << " with dimension " << fullnewton.dimension() << "\n";

  bool didParse = false;
  auto pu = sys->unit("mm", &didParse);
  std::cout << "Should be mm: " << pu << "\n";
  if (!didParse)
  {
    std::cerr << "ERROR: Could not parse 'mm'\n";
    ok = false;
  }

  auto measurement = sys->measurement("5 mm", &didParse);
  std::cout << "Should be 5 mm: " << measurement << "\n";
  if (!didParse)
  {
    std::cerr << "ERROR: Could not parse '5 mm'\n";
    ok = false;
  }

  // Let's visit all the units organized by their basic dimension.
  for (const auto& dimEntry : sys->m_indexByDimension)
  {
    std::cout << dimEntry.first << ":\n";
    for (const auto& entry : dimEntry.second)
    {
      std::cout << "  " << entry->m_name.data() << " (" << entry->m_symbol.data() << ")\n";
    }
  }

  ok &= testUnitFactorization(sys.get());

  ok &= testPluralForms();
  ok &= testWhitespaceParsing();
  ok &= testParenthesisParsing();

  return ok ? 0 : 1;
}
