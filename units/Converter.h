#ifndef units_Converter_h
#define units_Converter_h

#include "units/Unit.h"
#include "units/Measurement.h"

#include <functional>
#include <vector>

units_BEGIN_NAMESPACE

/// An object that can convert many numeric values assuming they all have the same units.
struct UNITS_EXPORT Converter : std::enable_shared_from_this<Converter>
{
  Converter(Unit from, Unit to);

  /// Apply a precomputed conversion, but only if \a measurement's units **exactly** match.
  Measurement transform(const Measurement& value) const;

  /// Apply a precomputed conversion assuming the value is specified in this converter's units.
  double transform(double value) const;

  /// Apply a precomputed conversion to all values in the input sequence and store them
  /// in the output sequence starting at \a outBegin.
  template<typename InputIterator, typename OutputIterator>
  void transform(InputIterator inBegin, InputIterator inEnd, OutputIterator outBegin) const
  {
    auto outIt = outBegin;
    for (auto inIt = inBegin; inIt != inEnd; ++inIt, ++outIt)
    {
      *outIt = this->transform(*inIt);
    }
  }

  Unit m_source;
  Unit m_target;

  /// An ordered sequence of transforms to apply to input values.
  std::vector<std::function<double(double)>> m_transforms;
  /// Any final scaling to match prefixes provided/requested by the source/target units.
  /// The values are (base, exponent) pairs.
  double m_scaling{ 1. };
};

units_CLOSE_NAMESPACE

#endif // units_Converter_h
