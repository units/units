#ifndef units_PreferredUnits_h
#define units_PreferredUnits_h

#include "units/Unit.h"

#include <memory>
#include <unordered_map>
#include <vector>

units_BEGIN_NAMESPACE

struct CompatibleUnitOptions;
struct System;

/// Preferences for how to prioritize units of a given dimension in user interfaces.
struct UNITS_EXPORT PreferredUnits : public std::enable_shared_from_this<PreferredUnits>
{
  struct UnitAndLabel
  {
    Unit m_unit;
    std::string m_label;
  };
  using PrioritizedUnits = std::vector<UnitAndLabel>;
  using UnitsByDimension = std::map<units::Dimension, PrioritizedUnits>;

  static std::shared_ptr<PreferredUnits> create(System* system, bool empty = false);
  std::shared_ptr<PreferredUnits> copy();

  /// Return all preferred units of any dimension.
  ///
  /// These are just the *first* unit of each dimension.
  std::vector<std::string> allPreferredUnits();

  /// Return all the suggested units of any dimension.
  ///
  /// This is a superset of allPreferredUnits() that includes
  /// suggested but not-necessarily-preferred units.
  std::vector<std::string> allSuggestedUnits();

  /// Return units suggested by the unit context given an exemplar unit.
  std::vector<Unit> suggestedUnits(const Unit& exemplar, CompatibleUnitOptions options);
  std::vector<std::string> suggestedUnits(const std::string& exemplar, CompatibleUnitOptions options);
  // std::vector<Unit> suggestedUnits(const Dimension& dimension);

  bool m_userEditable{ true };
  UnitsByDimension m_unitsByDimension;
  System* m_system{ nullptr };
};

units_CLOSE_NAMESPACE

#endif // units_PreferredUnits_h
